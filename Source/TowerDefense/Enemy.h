// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Enemy.generated.h"

UCLASS()
class TOWERDEFENSE_API AEnemy : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemy();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
		float maxHP;
	UPROPERTY(EditAnywhere)
	float currentHP;

	UPROPERTY(EditAnywhere)
	bool isBoss;
	UPROPERTY(EditAnywhere)
	bool isFlying;

	UFUNCTION(BlueprintImplementableEvent)
		void OnDie();

	UFUNCTION(BlueprintImplementableEvent)
		void OnKill();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere)
	TArray<AActor*> WaypointsArray;

	UFUNCTION()
		void SetWaypoint(TArray<AActor*> Waypoints);

	UFUNCTION()
		void Navigate();

	UPROPERTY(EditAnywhere)
		int CurrentWaypoint;

	UFUNCTION(BlueprintCallable)
		void TakeDamage();
	
	UFUNCTION(BlueprintCallable)
		void checkHP();

};
