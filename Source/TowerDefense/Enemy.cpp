// Fill out your copyright notice in the Description page of Project Settings.

#include "Enemy.h"
#include "EnemyAIController.h"
#include "Waypoint.h"
#include "PlayerCharacter.h"

// Sets default values
AEnemy::AEnemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();
	CurrentWaypoint = 1;
	currentHP = maxHP;
}

// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AEnemy::SetWaypoint(TArray<AActor*> Waypoints)
{
	WaypointsArray = Waypoints;
	Navigate();
}

void AEnemy::Navigate()
{
	AEnemyAIController* EnemyAIController = Cast<AEnemyAIController>(GetController());

	if (EnemyAIController)
	{
		if (CurrentWaypoint <= WaypointsArray.Num())
		{	
			for (AActor* Waypoint : WaypointsArray)
			{
				AWaypoint* WaypointItr = Cast<AWaypoint>(Waypoint);

				if (WaypointItr)
				{
					if (WaypointItr->GetWaypointOrder() == CurrentWaypoint)
					{
						EnemyAIController->MoveToActor(Waypoint, 5.f, false);
						CurrentWaypoint++;
						break;
					}
				}
			}
		}
		else
		{
			//APlayerCharacter.
			OnDie();
			this->Destroy();

		}
	}
}

void AEnemy::TakeDamage()
{
	currentHP--;
	checkHP();
}

void AEnemy::checkHP()
{
	if (currentHP <= 0)
	{
		OnKill();
		this->Destroy();
	}
}

