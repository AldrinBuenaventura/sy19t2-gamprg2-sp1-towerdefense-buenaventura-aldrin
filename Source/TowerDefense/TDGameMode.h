// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDGameMode.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FPlayerHit);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FPlayerKill);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FPlayerFinish);
/**
 * 
 */
UCLASS()
class TOWERDEFENSE_API ATDGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
		int maxHP;

	UPROPERTY(EditAnywhere)
		int currentHP;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int currentWave;

	


public:
	UPROPERTY(BlueprintAssignable, BlueprintCallable, EditAnywhere, Category = "EventDispatcher")
		FPlayerHit onTakeDamage;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, EditAnywhere, Category = "EventDispatcher")
		FPlayerKill onEnemyKill;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, EditAnywhere, Category = "EventDispatcher")
		FPlayerFinish onPlayerEnd;

	UPROPERTY(EditDefaultsOnly, Category = "WavesData")
		TSubclassOf<class UWavesData> WavesData;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<AActor*> Spawners;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Counter;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int i;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int currentDead;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int totalWave;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool isEnd;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<AActor> EnemyToSpawn;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int currentGold;

	UFUNCTION(BlueprintImplementableEvent)
		void InitializeWave();

	UFUNCTION(BlueprintImplementableEvent)
		void AddDelay();

	UFUNCTION(BlueprintImplementableEvent)
		void FinishWave();

	UFUNCTION(BlueprintCallable)
		void TakeDamage();

	UFUNCTION(BlueprintCallable)
		int GetCurrentHP();

	UFUNCTION(BlueprintCallable)
		void SpawnWave();

	UFUNCTION(BlueprintCallable)
		int GetCurrentWave();

	UFUNCTION(BlueprintCallable)
		void AddSpawner(AActor* Spawner);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class ASpawner* Spawner;

	UFUNCTION(BlueprintCallable)
		void addGold();
};
