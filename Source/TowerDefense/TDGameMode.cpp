// Fill out your copyright notice in the Description page of Project Settings.

#include "TDGameMode.h"
#include "Spawner.h"

void ATDGameMode::BeginPlay()
{
	Super::BeginPlay();
	currentWave = 0;
	currentHP = maxHP;	
}



void ATDGameMode::TakeDamage()
{
	currentHP--;
}

int ATDGameMode::GetCurrentHP()
{
	return currentHP;
}


void ATDGameMode::SpawnWave()
{
	InitializeWave();
	if (i < Counter)
	{
		int32 randomNumber = FMath::RandRange(0, Spawners.Num() - 1);
		Spawner = Cast<ASpawner>(Spawners[randomNumber]);
		AddDelay();
	}
}

int ATDGameMode::GetCurrentWave()
{
	return currentWave;
}

void ATDGameMode::AddSpawner(AActor * Spawner)
{
	Spawners.Add(Spawner);
}

void ATDGameMode::addGold()
{
	currentGold++;
}
