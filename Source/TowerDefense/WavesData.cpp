// Fill out your copyright notice in the Description page of Project Settings.

#include "WavesData.h"

int32 UWavesData::GetCount(int x)
{
	return Wavess[x].Count;
}

TSubclassOf<AActor> UWavesData::GetEnemy(int x)
{
	return Wavess[x].EnemyToSpawn;
}
