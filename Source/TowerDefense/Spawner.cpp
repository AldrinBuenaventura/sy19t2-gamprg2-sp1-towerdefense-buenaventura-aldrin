// Fill out your copyright notice in the Description page of Project Settings.

#include "Spawner.h"
#include "Engine/World.h"
#include "GameFramework/Actor.h"
#include "Enemy.h"

// Sets default values
ASpawner::ASpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
	Super::BeginPlay();
	//SpawnEnemy();
}

// Called every frame
void ASpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASpawner::SpawnEnemy(TSubclassOf<AActor> SpawnMe)
{
	FActorSpawnParameters SpawnParams;
	AActor* SpawnedActorRef = GetWorld()->SpawnActor<AActor>(SpawnMe, this->GetActorLocation(), FRotator(0, 0, 0), SpawnParams);
	UE_LOG(LogTemp, Warning, TEXT("ENEMY SPAWNED"));
	Enemy = Cast<AEnemy>(SpawnedActorRef);
	UE_LOG(LogTemp, Warning, TEXT("ENEMY CASTED"));
	Enemy->SetWaypoint(Actors);
	UE_LOG(LogTemp, Warning, TEXT("ENEMY WAYPOINTED"));
}



