// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Turret.generated.h"

UCLASS()
class TOWERDEFENSE_API ATurret : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATurret();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	class AEnemy* Enemy;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UStaticMeshComponent* StaticMesh;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class USphereComponent* SphereComponent;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class USceneComponent* SceneComponent;

	UFUNCTION(BlueprintCallable)
	void onOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION(BlueprintCallable)
		void endOverlap(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION(BlueprintCallable)
		void Attack();
	
	UFUNCTION(BlueprintCallable)
		void SpawnProjectile();



public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditDefaultsOnly, Category = "Spawner")
		TSubclassOf<AActor> ActorToSpawn;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<AActor*> EnemiesInRange;

	UFUNCTION(BlueprintImplementableEvent)
		void AddDelay();

	UFUNCTION(BlueprintCallable)
		void setState(bool state);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool isShooting;

	class AProjectile* Projectile;
};
