// Fill out your copyright notice in the Description page of Project Settings.

#include "Projectile.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Enemy.h"

// Sets default values
AProjectile::AProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMeshComponent");

	SetRootComponent(StaticMesh);
}

// Called when the game starts or when spawned
void AProjectile::BeginPlay()
{
	Super::BeginPlay();
	//StaticMesh->OnComponentHit.AddDynamic(this, &AProjectile::OnHit);
	StaticMesh->OnComponentBeginOverlap.AddDynamic(this, &AProjectile::onOverlap);
}

// Called every frame
void AProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	FVector Location = GetActorLocation();
	Location += GetActorForwardVector() * DeltaTime * Speed;
	SetActorLocation(Location);
}

void AProjectile::FollowEnemy(AActor * Target)
{
	SetActorRotation(UKismetMathLibrary::FindLookAtRotation(this->GetActorLocation(), Target->GetActorLocation()));
	//FindLookAtRotation

}

void AProjectile::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{

}

void AProjectile::onOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Enemy = Cast<AEnemy>(OtherActor);

	if (Cast<AEnemy>(OtherActor))
	{
		Enemy->TakeDamage();
		this->Destroy();
	}
}
