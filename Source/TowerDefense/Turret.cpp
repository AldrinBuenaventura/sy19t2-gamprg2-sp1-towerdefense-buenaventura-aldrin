// Fill out your copyright notice in the Description page of Project Settings.

#include "Turret.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "Components/SceneComponent.h"
#include "Enemy.h"
#include "Projectile.h"

// Sets default values
ATurret::ATurret()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SceneComponent = CreateDefaultSubobject<USceneComponent>("SceneComponent");

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("StaticMeshComponent");
	StaticMesh->SetupAttachment(SceneComponent);

	SphereComponent = CreateDefaultSubobject<USphereComponent>("SphereComponent");
	SphereComponent->SetupAttachment(SceneComponent);

	SetRootComponent(SceneComponent);
}

// Called when the game starts or when spawned
void ATurret::BeginPlay()
{
	Super::BeginPlay();
	SphereComponent->OnComponentBeginOverlap.AddDynamic(this, &ATurret::onOverlap);

	//decided to put this in BP because it doesnt read add dynamic here ?
	//SphereComponent->OnComponentEndOverlap.AddDynamic(this, &ATurret::endOverlap);
}

// Called every frame
void ATurret::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (EnemiesInRange.Num() > 0 && !isShooting)
	{
		isShooting = true;
		Attack();
	}
}

void ATurret::setState(bool state)
{
	isShooting = state;
}

void ATurret::onOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	Enemy = Cast<AEnemy>(OtherActor);

	if (Cast<AEnemy>(OtherActor))
	{
		EnemiesInRange.Add(OtherActor);
	}
}

void ATurret::endOverlap(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	Enemy = Cast<AEnemy>(OtherActor);

	if (Cast<AEnemy>(OtherActor))
	{
		EnemiesInRange.Remove(OtherActor);
	}
}

void ATurret::Attack()
{
	if (EnemiesInRange.Num() > 0)
	{
		AddDelay();
	}
	else
	{
		isShooting = false;
	}
}

void ATurret::SpawnProjectile()
{
	FActorSpawnParameters SpawnParams;
	if (EnemiesInRange.Num() > 0)
	{
		AActor* SpawnedActorRef = GetWorld()->SpawnActor<AActor>(ActorToSpawn, FVector(this->GetActorLocation().X, this->GetActorLocation().Y, this->GetActorLocation().Z + 1720), FRotator(0, 0, 0), SpawnParams);
		Projectile = Cast<AProjectile>(SpawnedActorRef);
		Projectile->FollowEnemy(EnemiesInRange[0]);
	}
}
