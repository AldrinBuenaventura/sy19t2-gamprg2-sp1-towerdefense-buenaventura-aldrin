// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "WavesData.generated.h"

/**
 * 
 */

USTRUCT()
struct FWaves
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere)
	TSubclassOf<AActor> EnemyToSpawn;

	UPROPERTY(EditAnywhere)
	int32 Count;
};

UCLASS(BlueprintType)
class TOWERDEFENSE_API UWavesData : public UDataAsset
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere)
	FWaves Waves;

	UPROPERTY(EditAnywhere)
	TArray<FWaves> Wavess;

	UFUNCTION(BlueprintCallable)
		int32 GetCount(int x);

	UFUNCTION(BlueprintCallable)
		TSubclassOf<AActor> GetEnemy(int x);
	
};
